import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
/*
   История - это по сути список. Тебе нужно создать обьект типа HistoryEntry с полями типа User, Book, Operation и поместить этот список в Библиотекаря.
   Operation - это enum со значениями TAKE, RETURN
   То есть история это List<HistoryEntry>
 */

public class HistoryEntry {

    private User user;
    private Book book;
    private Operation operation;

    public HistoryEntry(User user, Book book, Operation operation) {
        this.user = user;
        this.book = book;
        this.operation = operation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HistoryEntry that = (HistoryEntry) o;
        return Objects.equals(user, that.user) &&
                Objects.equals(book, that.book) &&
                operation == that.operation;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book, operation);
    }
}
