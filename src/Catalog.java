import java.util.List;

interface Catalog<T> {
    /*
    Убери public. Это избыточно. Все методы в интерфейса по умолчанию public
    Укороти названия. add, remove, exists...
     */
    public List<T> add(T type) throws LibraryException;
    public List<T> remove(T type) throws LibraryException;
    public void print(List<T> list);
    public boolean exist(T type);
}
