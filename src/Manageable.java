import java.util.List;
import java.util.Map;
/*
   Интерфейс должен быть следующим
   - добавить книгу в каталог книг
   - добавить автора в каталог авторов
   - добавить пользователя в каталог пользователей
   - выдать пользователю книгу
   - вывести полную информацию об авторе по его псевдониму
   - вывести историю выдач книг
 */
public interface Manageable {
    /* модификатор public в интерфейсах избыточен. Все методы обьявлены в интерфейсе уже паблик по умолчанию */
    public <T> void addToDirectories(List<T> catalogs);

    public  <T> void removeFromDirectories(List<T>catalogs);

    /*
     Здесь параметризация не нужна. Метод должен принимать Пользователя которому нужно выдать книгу и Книгу которую нужно выдать пользователю
     */
    public <Book> void giveBooksToUsers(List<Book>books);

    public <T> void booksWereGiven(List<T> books);

    /*
        Метод должен принимать обычный String
     */
    public <K,V> void identifyAuthorByPseudonym(Map<K,V> authorPseudonym);
}
