import java.util.Objects;

/*
   Выбери более удачное имя для класса
   Переопредели equals & hashCode для всех классов типа Автор, Книга, Пользователь
 */
public abstract class Name {
    private final String firstname;
    private final String secondname;
    private final String surname;

    public Name(String firstname, String secondname, String surname) {
        this.firstname = firstname;
        this.secondname = secondname;
        this.surname = surname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return "Name{" +
                "firstname='" + firstname + '\'' +
                ", secondname='" + secondname + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Name name = (Name) o;
        return Objects.equals(firstname, name.firstname) &&
                Objects.equals(secondname, name.secondname) &&
                Objects.equals(surname, name.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, secondname, surname);
    }
}
