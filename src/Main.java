import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {

    public static void main(String[] args) throws LibraryException {
        Librarian librarian = Librarian.getLibrarian();
        initialize(librarian);

        ;
        try {
            librarian.giveBookToUser(new User("Kateryna ", "Stanislavivna ", "Danko ",
                    new Date(1981, 10, 10)), new Book("Пожарный. ", 2016,
                    680, "Джо Хилл"));
        } catch (LibraryException e) {
            e.printStackTrace();
        }

//        try {
//            librarian.giveBookToUser(new User("Kate ", "Stanislavivna ", "Danko ",
//                    new Date(1987, 10, 15)), new Book("Пожарный. ", 2018,
//                    70, "Джо Хилл"));
//        } catch (LibraryException e) {
//            e.printStackTrace();
//        }
    }

    private static void initialize(Librarian librarian) {
        librarian.addBook(new Book("Жорж Санд. История моей жизни. ", 1990, 33, "Жорж Санд"));
        librarian.addBook(new Book("Пожарный. ", 2016, 680, "Джо Хилл"));
        librarian.addAuthor(new Author("Джозеф", "Хиллстром", "Кинг", "Джо Хилл"));
        librarian.addAuthor(new Author("Мэри", "Энн", "Эванс", "Джордж Эллиот"));
        librarian.addAuthor(new Author("Надежда", "Александровна", "Лохвицкая", "Тэффи"));
        librarian.addAuthor(new Author("Амандина", "Аврора", "Люсиль Дюпен", "Жорж Санд"));
        librarian.addUser(new User("Kateryna ", "Stanislavivna ", "Danko ", new Date(1981, 10, 10)));
        librarian.addUser(new User("Larysa ", "Vitaliivna ", "Smaznova ", new Date(1952, 10, 25)));
        librarian.addUser(new User("Igor ", "Andiyovich ", "Zinchenko ", new Date(1980, 10, 15)));
        librarian.addUser(new User("Danylo ", "Sergiyovich ", "Danko ", new Date(2009, 3, 10)));
        librarian.addUser(new User("Lina ", "Pavlivna ", "Kotlyk ", new Date(1959, 6, 12)));
    }
}
