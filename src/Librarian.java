import java.util.ArrayList;
import java.util.List;

/*
   Библиотакарь должен иметь свой интерфейс в нем должны быть методы управления каталогами а так же функции обозначены в задании
 */
public class Librarian implements LibrarianInterface {
    private static Librarian librarian;
        /* не нужно создавать данные при создании библиотекаря. Давай делать наш код более универсальным
           Код библиотекаря ничего не должен знать о данных. наполнить каталоги данными можно через специальные методы add интерфейса Librarian
         */

    private final AuthorCatalog allAuthors;
    private final UserCatalog allUsers;
    private final BookCatalog booksInLibrary;
    private final BookCatalog takenBooks;
    private final List<HistoryEntry> history;

    /*
   Конструктор записывается после полей класса. сместить вниз
    */
    private Librarian() throws LibraryException {
        this.allUsers = new UserCatalog();
        this.booksInLibrary = new BookCatalog();
        this.takenBooks = new BookCatalog();
        this.allAuthors = new AuthorCatalog();
        this.history = new ArrayList<>();
    }

    /*
    Методы записываются после полей класса и конструктора. сместить вниз
     */
    public static Librarian getLibrarian() throws LibraryException {
        if (librarian == null) {
            librarian = new Librarian();
        }
        return librarian;
    }

    public AuthorCatalog getAllAuthors() {
        return allAuthors;
    }

    public UserCatalog getAllUsers() {
        return allUsers;
    }

    public BookCatalog getBooksInLibrary() {
        return booksInLibrary;
    }

    public BookCatalog getTakenBooks() {
        return takenBooks;
    }

    @Override
    public void giveBookToUser(User user, Book book) throws LibraryException {
        if (!allUsers.exist(user)) {
            throw new LibraryException("There is no such user in userCatalog");
        }
        if (!booksInLibrary.exist(book)) {
            throw new LibraryException("There is no such book in bookCatalog");
        }
        booksInLibrary.remove(book);
        takenBooks.add(book);
        user.addBookToUser(book);

        logOperation(user, book, Operation.TAKE);
        /*
        Вот где-то здесь нужно записать в историю с Operation.TAKE
         */
        System.out.println("User: " + user + "get from library book: " + book);
    }

    private void logOperation(User user, Book book, Operation operation) {
        history.add(new HistoryEntry(user, book, operation));
    }

    @Override
    public void returnBookToLibrary(User user, Book book) throws LibraryException {
        if (!booksInLibrary.exist(book)) {
            throw new LibraryException("There is no such book in bookCatalog");
        }
        user.removeBookFromUser(book);
        takenBooks.remove(book);

        booksInLibrary.add(book);

        logOperation(user, book, Operation.RETURN);
         /*
        Вот где-то здесь нужно записать в историю с Operation.RETURN
         */
        System.out.println("User: " + user + "return to library book: " + book);
    }

    @Override
    public void findAuthorByCreativePseudonym(String creativePseudonym) {
        allAuthors.findAuthor(creativePseudonym);
    }

    @Override
    public void addAuthor(Author author) {

    }

    @Override
    public void addBook(Book book) {

    }

    @Override
    public void addUser(User user) {

    }

    @Override
    public List<HistoryEntry> getHistory() {
return getHistory();
    }
}
