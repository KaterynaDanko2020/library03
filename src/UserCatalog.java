import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserCatalog implements Catalog<User> {
    private List<User> users = new ArrayList<>();

    public List<User> getUsers() {
        return users;
    }

    @Override
    public List<User> add(User user) {
        users.add(user);
        return users;
    }

    @Override
    public List<User> remove(User user) throws LibraryException {
        users.remove(user);
        return users;
    }

    @Override
    public void print(List<User> catalog) {
        for (User user : users) {
            System.out.println(user);
        }
    }

    @Override
    public boolean exist(User user) {
        for (User u : users) {
            if (user.equals(u)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "UserCatalog{" +
                "users: " + users +
                '}';
    }

}

