import java.util.Objects;

public class Book {
    private String title;
    private String creativePseudonym;
    private int year;
    private int pages;


    public Book(String title, int year, int pages, String creativePseudonym) {
        this.title = title;
        this.creativePseudonym = creativePseudonym;
        this.year = year;
        this.pages = pages;
    }

    public String getTitle() {
        return title;
    }

    public String getCreativePseudonym() {
        return creativePseudonym;
    }

    public int getYear() {
        return year;
    }

    public int getPages() {
        return pages;
    }

//    public boolean existAuthorInAuthorCatalog(String creativePseudonym) {
//        for (String this.creativePseudonym : AuthorCatalog.getAuthors()) {
//            if (this.creativePseudonym.equals(creativePseudonym)) {
//                return true;
//            }
//        }
//        return false;
//    }

    @Override
    public String toString() {
        return "Book{" +
                " title: " + title +
                ", creativePseudonym: " + creativePseudonym +
                ", year: " + year +
                ", pages: " + pages +
                "\n" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return year == book.year &&
                pages == book.pages &&
                Objects.equals(title, book.title) &&
                Objects.equals(creativePseudonym, book.creativePseudonym);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, creativePseudonym, year, pages);
    }
}
