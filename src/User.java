import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class User extends Name{

    private Date dateOfBirth;
    List<Book> takenBooks = new ArrayList<>();

    public User(String firstname, String secondname, String surname, Date dateOfBirth) {
       super (firstname, secondname, surname);
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateOfBirth() {
        return new Date(dateOfBirth.getTime());
    }


    public List<Book> addBookToUser(Book book) {
        takenBooks.add(book);
        return takenBooks;
    }
    public List<Book> removeBookFromUser(Book book) {
        takenBooks.remove(book);
        return takenBooks;
    }

    public List<Book> getTakenBooks() {
        return takenBooks;
    }

    @Override
    public String toString() {
        return "User{" +
                " first name: " + super.getFirstname() +
                ", second name: " + super.getSecondname() +
                ", surname: " + super.getSurname() +
                ", dateOfBirth: " + dateOfBirth +
                ", takenBooks: " + takenBooks + '\'' +
                "\n" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return Objects.equals(dateOfBirth, user.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateOfBirth);
    }
}
