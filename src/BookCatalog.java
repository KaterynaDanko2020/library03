import java.util.ArrayList;
import java.util.List;

public class BookCatalog implements Catalog<Book> {
    List<Book> books = new ArrayList<>();

    public List<Book> addBook(Book book, AuthorCatalog catalog) throws LibraryException {
        books.add(book);
        if (!catalog.existAuthor(book.getCreativePseudonym())) {
            throw new LibraryException();
        }
        return books;
    }

    @Override
    public List<Book> add(Book type) throws LibraryException {
        return null;
    }

    @Override
        public List<Book> remove(Book book) throws LibraryException {
            books.remove(book);
            return books;
        }

        @Override
        public void print(List <Book> books) {
            for (Book b : books) ;
            System.out.println(books);
        }
        @Override
        public boolean exist(Book book){
            for (Book b : books) {
                if (book.equals(b)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString () {
            return "BookCatalog{" +
                    " books: " + books +
                    '}';

        }
    }



