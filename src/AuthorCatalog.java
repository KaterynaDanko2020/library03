import java.util.ArrayList;
import java.util.List;

public class AuthorCatalog implements Catalog<Author> {
    List<Author> authors = new ArrayList<>();

    public List<Author> getAuthors() {
        return authors;
    }

    @Override
    public List<Author> add(Author author) {
        authors.add(author);
        return authors;
    }

    @Override
    public List<Author> remove(Author author) throws LibraryException {
        authors.remove(author);
        return authors;
    }

    @Override
    public void print(List<Author> authors) {
        for (Author authorList : authors) {
            System.out.println(authorList);
        }
    }

    @Override
    public boolean exist(Author type) {
        return false;
    }

    public boolean existAuthor(String creativePseudonym) {
        for (Author a : authors) {
            if (a.getCreativePseudonym().equals(creativePseudonym)) {
                return true;
            }
        }
        return false;
    }

    public void findAuthor(String creativePseudonym) {
        for (Author author : authors) {
            if (author.getCreativePseudonym().equals(creativePseudonym))
                System.out.println(author);
        }
    }
    @Override
    public String toString() {
        return "AuthorCatalog{" +
                "authors: " + authors +
                '}';
    }

}