import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Author extends Name{

    public final String creativePseudonym;

    public Author(String firstname, String secondname, String surname, String creativePseudonym) {
        super(firstname,secondname,surname);
        this.creativePseudonym = creativePseudonym;
    }

    public String getCreativePseudonym() {
        return creativePseudonym;
    }

    @Override
    public String toString() {
        return "Author{" +
                " first name: " + super.getFirstname() +
                ", second name: " + super.getSecondname() +
                ", surname: " + super.getSurname() +
                ", creativePseudonym: '" + creativePseudonym + '\'' +
                "\n" +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Author author = (Author) o;
        return Objects.equals(creativePseudonym, author.creativePseudonym);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), creativePseudonym);
    }
}
