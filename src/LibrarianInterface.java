import java.util.List;

public interface LibrarianInterface {
    void giveBookToUser(User user, Book book) throws LibraryException;

    void returnBookToLibrary(User user, Book book) throws LibraryException;

    void findAuthorByCreativePseudonym(String creativePseudonym);

    void addAuthor(Author author);

    void addBook(Book book);

    void addUser(User user);

    List<HistoryEntry> getHistory();
}


//    public Book takeBook(User user, Book book) {
//       user.takenBooks.add(book);
//        System.out.println("User: " + user + "get from library book: " + book);
//        return (Book) user.takenBooks;
//    }
//
//    public Book returnBook(User user, Book book) {
//        user.takenBooks.remove(book);
//        System.out.println("User: " + user + "return to library book: " + book);
//        return (Book) user.takenBooks;
//    }

//            allUsers.add(new User("Kateryna ","Stanislavivna ","Danko ",new Date(1981,10,10)));
//            allUsers.add(new
//
//            User("Larysa ","Vitaliivna ","Smaznova ",new Date(1952,10,25)));
//            allUsers.add(new
//
//            User("Igor ","Andiyovich ","Zinchenko ",new Date(1980,10,15)));
//            allUsers.add(new
//
//            User("Danylo ","Sergiyovich ","Danko ",new Date(2009,3,10)));
//            allUsers.add(new
//
//            User("Lina ","Pavlivna ","Kotlyk ",new Date(1959,6,12)));
//            booksInLibrary.add(new
//
//            Book("Жорж Санд. История моей жизни. ",1990,33,"Жорж Санд"));
//            booksInLibrary.add(new
//
//            Book("Пожарный. ",2016,680,"Джо Хилл"));
//
////        if (!allAuthors.existInCatalog(creativePseudonym)) {
////            throw new LibraryException("There is no such author in authorCatalog");
////        }
//            allAuthors.add(new
//
//            Author("Амандина","Аврора","Люсиль Дюпен","Жорж Санд"));
//            allAuthors.add(new
//
//            Author("Джозеф","Хиллстром","Кинг","Джо Хилл"));
//            allAuthors.add(new
//
//            Author("Мэри","Энн","Эванс","Джордж Эллиот"));
//            allAuthors.add(new
//
//            Author("Надежда","Александровна","Лохвицкая","Тэффи"));